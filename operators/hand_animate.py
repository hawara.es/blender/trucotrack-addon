import bpy
from mathutils import Matrix, Vector
from ..rigs import rigify_hand, rain_hand
from ..mediapipe import hand as mpp_hand

class OT_TrucoTrackHandAnimate(bpy.types.Operator):
    """Animates an armature so it follows a loaded collection of empties"""

    bl_idname = "armature.trucotrack_hand_animate"
    bl_label = "Animate the hand of an armature"
    bl_options = {'REGISTER', 'UNDO'}

    rig_type: bpy.props.EnumProperty(
        name = "Rig type",
        description = "Type of rig that will be animated",
        items = [
            ('rigify', 'Rigify', 'Standard rig generated from a metarig'),
            ('rain_rig', 'Rain rig', 'Rig used in Rain v2'),
        ],
        default = 'rain_rig',
    )

    rescale: bpy.props.BoolProperty(
        name = "Rescale",
        description = "Rescale the empties so they match the armature size",
        default = True,
    )

    @classmethod
    def poll(cls, context):
        if not bpy.context.mode == 'OBJECT':
            return False
        if not bpy.context.active_object:
            return False
        if not len(bpy.context.selected_objects) == 2:
            return False
        for object in bpy.context.selected_objects:
            if not object.name == bpy.context.active_object.name:
                if not object['mpp_bone'] == 'wrist':
                    return False
        return True

    def execute(self, context):
        if self.rig_type == 'rigify':
            self.rig_hand = rigify_hand
        elif self.rig_type == 'rain_rig':
            self.rig_hand = rain_hand

        if not self.rig_hand.is_a_valid_armature(bpy.context.active_object):
            return {'CANCELLED'}

        self.rig = bpy.context.active_object

        for object in bpy.context.selected_objects:
            if not object.name == self.rig.name:
                if object['mpp_bone'] == 'wrist':
                    self.wrist = object

        collection_name = self.wrist['mpp_collection']
        self.collection = bpy.data.collections[collection_name]

        if self.rescale:
            self._fix_scale()
        self._animate()

        return {'FINISHED'}

    def menu_func(self, context):
        self.layout.operator(OT_TrucoTrackHandAnimate.bl_idname)

    def _get_suffix(self):
        if self.wrist['mpp_handedness'] == "left":
            return "L"
        else:
            return "R"

    def _get_rig_scale_reference(self):
        handedness = self.wrist['mpp_handedness']
        bone_from = self.rig_hand.find_wrist_bone(handedness, self.rig)
        bone_to = list(self.rig_hand.find_middle_bones(handedness, self.rig))[0]
        reference_from = self.rig.data.bones[bone_from].tail
        reference_to = self.rig.data.bones[bone_to].head
        return (reference_to - reference_from).length

    def _get_collection_scale_reference(self):
        finger_from = mpp_hand.get_wrist_bone()
        finger_to = list(mpp_hand.get_middle_bones())[0]
        bone_from = '{collection}: {finger_from}'.format(
            collection=self.collection.name,
            finger_from=finger_from)
        bone_to = '{collection}: {finger_to}'.format(
            collection=self.collection.name,
            finger_to=finger_to)
        reference_from = self.collection.objects[bone_from].location
        reference_to = self.collection.objects[bone_to].location
        return (reference_to - reference_from).length

    def _fix_scale(self):
        collection_size = self._get_collection_scale_reference()
        rig_size = self._get_rig_scale_reference()
        location, rotation, scale = self.wrist.parent.matrix_world.decompose()
        rescale = rig_size / collection_size
        self.wrist.parent.matrix_world = get_location_matrix(location) @ \
            get_rotation_matrix(rotation) @ \
            get_scale_matrix(Vector((1, 1, 1)) * rescale)

    def _animate(self):
        self._add_constraints()

    def _add_constraints(self):
        handedness = self.wrist['mpp_handedness']
        matches = self.rig_hand.get_mediapipe_matches(handedness, self.rig)
        for reference in self.collection.objects:
            if not 'mpp_bone' in reference:
                continue
            mpp_bone = reference['mpp_bone']
            if not mpp_bone in matches:
                continue
            rig_bone = self.rig.pose.bones[matches[mpp_bone]]
            rotation_constraint = rig_bone.constraints.new(type='COPY_ROTATION')
            rotation_constraint.target = reference
            if mpp_bone == 'wrist':
                location_constraint = rig_bone.constraints.new(type='COPY_LOCATION')
                location_constraint.target = reference
            else:
                rotation_constraint.use_y = False

def get_location_matrix(location):
    return Matrix.Translation(location)

def get_rotation_matrix(rotation):
    return rotation.to_matrix().to_4x4()

def get_scale_matrix(scale_vector):
    scale_matrix = Matrix()
    for axis, scale_axis in enumerate(scale_vector):
        scale_matrix[axis][axis] = scale_axis
    return scale_matrix

def register():
    bpy.utils.register_class(OT_TrucoTrackHandAnimate)
    bpy.types.VIEW3D_MT_object.append(OT_TrucoTrackHandAnimate.menu_func)

def unregister():
    bpy.utils.unregister_class(OT_TrucoTrackHandAnimate)
    bpy.types.VIEW3D_MT_object.remove(OT_TrucoTrackHandAnimate.menu_func)
