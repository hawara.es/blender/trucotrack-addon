import os
import bpy
from math import sqrt
from mathutils import Vector, Matrix
from ..trucotrack import hand_csv
from ..mediapipe import hand as mpp_hand

class OT_TrucoTrackHandLoad(bpy.types.Operator):
    """Load hand armature and poses from a tracking file"""

    bl_idname = "armature.trucotrack_hand_load"
    bl_label = "Load hand armature and poses"
    bl_options = {'REGISTER', 'UNDO'}

    filepath: bpy.props.StringProperty(
        name = "File",
        subtype = 'FILE_PATH',
        default = "",
    )

    first_hand: bpy.props.IntProperty(
        name = "First hand",
        description = "Index of the first hand to load",
        default = 0,
    )

    second_hand: bpy.props.IntProperty(
        name = "Second hand",
        description = "Index of the second hand to load",
        default = 1,
    )

    start_at_frame: bpy.props.IntProperty(
        name = "Start at frame",
        description = "First frame to read",
        default = 1,
    )

    jump_size: bpy.props.IntProperty(
        name = "Jump size",
        description = "Number of frames to skip for every step",
        default = 1,
    )

    reset_animation_frames: bpy.props.BoolProperty(
        name = "Reset animation frames",
        description = "Reset the frame start and frame end so it matches the file",
        default = True,
    )

    scale: bpy.props.FloatProperty(
        name = "Scale",
        description = "Scale factor to be applied",
        default = 0.01,
    )

    flip_x: bpy.props.BoolProperty(
        name = "Flip horizontally",
        description = "Flip the horizontal (X) axis",
        default = True,
    )

    flip_y: bpy.props.BoolProperty(
        name = "Flip vertically",
        description = "Flip the vertical (Y) axis",
        default = True,
    )

    flip_z: bpy.props.BoolProperty(
        name = "Flip frontally",
        description = "Flip the frontal (Z) axis",
        default = True,
    )

    @classmethod
    def poll(cls, context):
        return bpy.context.mode == 'OBJECT'

    def execute(self, context):
        if self.filepath == "":
            return {'FINISHED'}

        self.abspath = bpy.path.abspath(self.filepath)

        collection_names = {}

        for row_id, row in enumerate(hand_csv.get_rows(self.abspath)):
            self.current_row = row

            if not self.current_row['hand_index'] in [self.first_hand, self.second_hand]:
                continue

            if (self.current_row['frame_number'] - self.start_at_frame) % self.jump_size != 0:
                continue

            if self.reset_animation_frames:
                if (self.current_row['frame_number'] == self.start_at_frame):
                    bpy.context.scene.frame_start = self.current_row['frame_number']
                    bpy.context.scene.frame_current = bpy.context.scene.frame_start
                if self.current_row['frame_number'] > bpy.context.scene.frame_end:
                    bpy.context.scene.frame_end = self.current_row['frame_number']

            hand_id = self.current_row['hand_index']

            if not hand_id in collection_names:
                collection_names[hand_id] = self._create_hand_collection()

            collection_name = collection_names[hand_id]
            parent = self._get_or_create_parent(collection_name)
            self._create_or_move_empties(collection_name, parent)

        return {'FINISHED'}

    def menu_func(self, context):
        self.layout.operator(OT_TrucoTrackHandLoad.bl_idname)

    def _flips(self) -> Vector:
        return Vector((
            1 if not self.flip_x else -1,
            1 if not self.flip_y else -1,
            1 if not self.flip_z else -1,))

    def _get_hand_name(self) -> str:
        return "Hand %s" % self.current_row['hand_index']

    def _create_hand_collection(self) -> str:
        collection = bpy.data.collections.new(name=self._get_hand_name())
        bpy.context.scene.collection.children.link(collection)
        return collection.name

    def _get_or_create_parent(self, collection_name: str):
        parent_name = "{collection}: Container".format(collection=collection_name)
        if parent_name in bpy.data.collections[collection_name].objects:
            return bpy.data.collections[collection_name].objects[parent_name]
        parent = bpy.data.objects.new(parent_name, None)
        parent.empty_display_type = 'CUBE'
        width = self.current_row['frame_width']
        height = self.current_row['frame_height']
        parent.empty_display_size = (width / 2) * self.scale
        parent.location = Vector((width / 2, height / 2, 0)) * self._flips() * self.scale
        bpy.data.collections[collection_name].objects.link(parent)
        return parent

    def _create_or_move_empties(self, collection_name: str, parent: bpy.types.Object):
        for bone_name in mpp_hand.get_bones():
            reference = self._get_or_create_reference(bone_name, collection_name, parent)
            self._set_custom_properties(reference, bone_name, collection_name)
            self._move_reference(reference)
        self._set_rotations()

    def _get_or_create_reference(self, bone_name: str, collection_name: str, parent: bpy.types.Object):
        candidate_name = "%s: %s" % (collection_name, bone_name)
        if not candidate_name in bpy.data.collections[collection_name].objects:
            reference = bpy.data.objects.new(candidate_name, None)
            reference.parent = parent
            if bone_name == 'wrist':
                reference.empty_display_type = 'ARROWS'
                reference.rotation_mode = 'QUATERNION'
                reference.empty_display_size = 50 * self.scale
            elif bone_name in [
                'thumb_cmc', 'thumb_mcp', 'thumb_ip',
                'index_finger_mcp', 'index_finger_pip', 'index_finger_dip',
                'middle_finger_mcp', 'middle_finger_pip', 'middle_finger_dip',
                'ring_finger_mcp', 'ring_finger_pip', 'ring_finger_dip',
                'pinky_mcp', 'pinky_pip', 'pinky_dip']:
                reference.empty_display_type = 'ARROWS'
                reference.rotation_mode = 'QUATERNION'
                reference.empty_display_size = 10 * self.scale
            else:
                reference.empty_display_type = 'SPHERE'
                reference.empty_display_size = 5 * self.scale
            bpy.data.collections[collection_name].objects.link(reference)
        else:
            reference = bpy.data.objects[candidate_name]
        return reference

    def _set_custom_properties(self, reference, bone_name, collection_name):
        reference['mpp_collection'] = collection_name
        reference['mpp_bone'] = bone_name
        if bone_name == "wrist":
            reference['mpp_handedness'] = self.current_row['hand_handedness'].lower()

    def _move_reference(self, reference):
        bone_name = reference['mpp_bone']
        world_location = self.current_row[bone_name] * self._flips() * self.scale
        reference.location = world_location - reference.parent.location
        reference.keyframe_insert(data_path="location", frame=self.current_row['frame_number'])

    def _set_rotations(self):
        hand_name = self._get_hand_name()
        frame_number = self.current_row['frame_number']
        self._set_wrist_rotation()
        mpp_hand.set_thumb_rotations(hand_name, frame_number)
        mpp_hand.set_index_rotations(hand_name, frame_number)
        mpp_hand.set_middle_rotations(hand_name, frame_number)
        mpp_hand.set_ring_rotations(hand_name, frame_number)
        mpp_hand.set_pinky_rotations(hand_name, frame_number)

    def _set_wrist_rotation(self):
        hand_name = self._get_hand_name()
        frame_number = self.current_row['frame_number']
        mpp_hand.set_wrist_rotation(hand_name, frame_number)

def register():
    bpy.utils.register_class(OT_TrucoTrackHandLoad)
    bpy.types.VIEW3D_MT_object.append(OT_TrucoTrackHandLoad.menu_func)

def unregister():
    bpy.utils.unregister_class(OT_TrucoTrackHandLoad)
    bpy.types.VIEW3D_MT_object.remove(OT_TrucoTrackHandLoad.menu_func)
