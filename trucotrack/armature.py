import bpy

def get_matrix_world(armature_object: bpy.types.Object, bone_name: str):
    # Source: https://blender.stackexchange.com/a/121495
    pose_bone = armature_object.pose.bones[bone_name]
    return armature_object.matrix_world @ pose_bone.matrix
