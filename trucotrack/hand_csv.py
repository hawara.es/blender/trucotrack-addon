import gzip
import csv
from mathutils import Vector
from ..mediapipe import hand

def fields():
    basic_fields = [
        'frame_number', 'frame_width', 'frame_height',
        'hand_index', 'hand_score', 'hand_handedness']

    bone_fields = []
    for bone_name in hand.get_bones():
        bone_fields.append("%s_x" % bone_name)
        bone_fields.append("%s_y" % bone_name)
        bone_fields.append("%s_z" % bone_name)

    return basic_fields + bone_fields

def is_valid_header(header: list):
    return len(set(header) & set(fields())) == len(fields())

def get_rows(filepath: str):
    try:
        with gzip.open(filepath, "rt") as file:
            data = csv.DictReader(file, delimiter = ",")
            header = None
            for row in data:
                if not header:
                    header = list(row.keys())
                if not is_valid_header(header):
                    return None
                yield ensure_types(row)
        file.close()
    except:
        return None

def ensure_types(row: dict):
    for field in row:
        if field in ['frame_number', 'frame_width', 'frame_height', 'hand_index']:
            row[field] = int(row[field])
        elif field in ['hand_handedness']:
            row[field] = str(row[field])
        else:
            row[field] = float(row[field])
    for bone_name in hand.get_bones():
        x_key = "%s_x" % bone_name
        y_key = "%s_y" % bone_name
        z_key = "%s_z" % bone_name

        vector = Vector((row[x_key], row[y_key], row[z_key]))
        row[bone_name] = vector

        del row[x_key]
        del row[y_key]
        del row[z_key]
    return row
