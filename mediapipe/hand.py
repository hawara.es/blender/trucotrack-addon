import bpy
from mathutils import Vector, Matrix, Euler

def get_wrist_bone():
    return 'wrist'

def get_thumb_bones():
    for suffix in ['cmc', 'mcp', 'ip', 'tip']:
        yield 'thumb_' + suffix

def get_finger_bones(finger: str):
    for suffix in ['mcp', 'pip', 'dip', 'tip']:
        yield finger + '_' + suffix

def get_index_bones():
    yield from get_finger_bones('index_finger')

def get_middle_bones():
    yield from get_finger_bones('middle_finger')

def get_ring_bones():
    yield from get_finger_bones('ring_finger')

def get_pinky_bones():
    yield from get_finger_bones('pinky')

def get_bones():
    yield get_wrist_bone()
    yield from get_thumb_bones()
    yield from get_index_bones()
    yield from get_middle_bones()
    yield from get_ring_bones()
    yield from get_pinky_bones()

def get_quaternion(direction: Vector, side: Vector) -> Matrix:
    rotation_matrix = Matrix.Identity(3)
    rotation_matrix.col[0] = side
    rotation_matrix.col[1] = direction
    rotation_matrix.col[2] = side.cross(direction)
    return rotation_matrix.to_quaternion()

def get_direction(hand_name: str, bone_from: str, bone_to: str) -> Vector:
    direction_from = bpy.data.objects['%s: %s' % (hand_name, bone_from)]
    direction_to = bpy.data.objects['%s: %s' % (hand_name, bone_to)]
    return direction_to.location - direction_from.location

def set_thumb_rotations(hand_name: str, frame_number: int):
    # first
    direction = get_direction(hand_name, 'thumb_cmc', 'thumb_mcp')
    object = bpy.data.objects['%s: thumb_cmc' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # second
    direction = get_direction(hand_name, 'thumb_mcp', 'thumb_ip')
    object = bpy.data.objects['%s: thumb_mcp' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # third
    direction = get_direction(hand_name, 'thumb_ip', 'thumb_tip')
    object = bpy.data.objects['%s: thumb_ip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)

def set_index_rotations(hand_name: str, frame_number: int):
    # first
    direction = get_direction(hand_name, 'index_finger_mcp', 'index_finger_pip')
    object = bpy.data.objects['%s: index_finger_mcp' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # second
    direction = get_direction(hand_name, 'index_finger_pip', 'index_finger_dip')
    object = bpy.data.objects['%s: index_finger_pip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # third
    direction = get_direction(hand_name, 'index_finger_dip', 'index_finger_tip')
    object = bpy.data.objects['%s: index_finger_dip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)

def set_middle_rotations(hand_name: str, frame_number: int):
    # first
    direction = get_direction(hand_name, 'middle_finger_mcp', 'middle_finger_pip')
    object = bpy.data.objects['%s: middle_finger_mcp' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # second
    direction = get_direction(hand_name, 'middle_finger_pip', 'middle_finger_dip')
    object = bpy.data.objects['%s: middle_finger_pip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # third
    direction = get_direction(hand_name, 'middle_finger_dip', 'middle_finger_tip')
    object = bpy.data.objects['%s: middle_finger_dip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)

def set_ring_rotations(hand_name: str, frame_number: int):
    palm_normal = get_palm_normal(hand_name)
    # first
    direction = get_direction(hand_name, 'ring_finger_mcp', 'ring_finger_pip')
    object = bpy.data.objects['%s: ring_finger_mcp' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # second
    direction = get_direction(hand_name, 'ring_finger_pip', 'ring_finger_dip')
    object = bpy.data.objects['%s: ring_finger_pip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # third
    direction = get_direction(hand_name, 'ring_finger_dip', 'ring_finger_tip')
    object = bpy.data.objects['%s: ring_finger_dip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)

def set_pinky_rotations(hand_name: str, frame_number: int):
    palm_normal = get_palm_normal(hand_name)
    # first
    direction = get_direction(hand_name, 'pinky_mcp', 'pinky_pip')
    object = bpy.data.objects['%s: pinky_mcp' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # second
    direction = get_direction(hand_name, 'pinky_pip', 'pinky_dip')
    object = bpy.data.objects['%s: pinky_pip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
    # third
    direction = get_direction(hand_name, 'pinky_dip', 'pinky_tip')
    object = bpy.data.objects['%s: pinky_dip' % hand_name]
    object.rotation_quaternion = direction.to_track_quat('Y', 'X')
    object.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)

def get_palm_normal(hand_name: str) -> Vector:
    wrist = bpy.data.objects['%s: wrist' % hand_name]
    pinky = bpy.data.objects['%s: pinky_mcp' % hand_name]
    index = bpy.data.objects['%s: index_finger_mcp' % hand_name]
    palm_normal = (pinky.location - wrist.location).cross(index.location - wrist.location)
    palm_normal.normalize()
    return palm_normal

def get_palm_direction(hand_name: str) -> Vector:
    middle = bpy.data.objects['%s: middle_finger_mcp' % hand_name]
    wrist = bpy.data.objects['%s: wrist' % hand_name]
    return middle.location - wrist.location

def set_wrist_rotation(hand_name: str, frame_number: int):
    direction = get_palm_direction(hand_name)
    palm_normal = get_palm_normal(hand_name)
    wrist = bpy.data.objects['%s: wrist' % hand_name]
    wrist.rotation_quaternion = get_quaternion(direction, palm_normal)
    wrist.keyframe_insert(data_path="rotation_quaternion", frame=frame_number)
