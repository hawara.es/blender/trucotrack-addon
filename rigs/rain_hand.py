import bpy
from ..mediapipe import hand as mpp_hand

def get_suffix(handedness: str = 'left') -> str:
    suffixes = {'left': 'L', 'right': 'R'}
    return suffixes[handedness]

def finger_bones(finger: str, handedness: str = 'left'):
    for number in ['1', '2', '3']:
        yield "FK-{finger}{number}.{suffix}".format(
            finger=finger,
            number=number,
            suffix=get_suffix(handedness))

def find_wrist_bone(handedness: str, rig: bpy.types.Object):
    bone_pattern = 'IK-Hand_Parent.{suffix}'
    bone_name = bone_pattern.format(suffix=get_suffix(handedness))
    if bone_name in rig.pose.bones:
        return bone_name

def find_finger_bones(finger: str, handedness: str, rig: bpy.types.Object):
    for bone_name in finger_bones(finger, handedness):
        if bone_name in rig.pose.bones:
            yield bone_name

def find_thumb_bones(handedness: str, rig: bpy.types.Object):
    yield from find_finger_bones('Thumb', handedness, rig)

def find_index_bones(handedness: str, rig: bpy.types.Object):
    yield from find_finger_bones('Index', handedness, rig)

def find_middle_bones(handedness: str, rig: bpy.types.Object):
    yield from find_finger_bones('Middle', handedness, rig)

def find_ring_bones(handedness: str, rig: bpy.types.Object):
    yield from find_finger_bones('Ring', handedness, rig)

def find_pinky_bones(handedness: str, rig: bpy.types.Object):
    yield from find_finger_bones('Pinky', handedness, rig)

def find_bones(handedness: str, rig: bpy.types.Object):
    yield find_wrist_bone(handedness, rig)
    yield from find_thumb_bones(handedness, rig)
    yield from find_index_bones(handedness, rig)
    yield from find_middle_bones(handedness, rig)
    yield from find_ring_bones(handedness, rig)
    yield from find_pinky_bones(handedness, rig)

def is_a_valid_armature(rig: bpy.types.Object):
    if not rig:
        return False
    if not rig.type == 'ARMATURE':
        return False
    if not hasattr(rig.data, "bones"):
        return False
    found_left_bones = sum(1 for x in find_bones('left', rig))
    found_right_bones = sum(1 for x in find_bones('right', rig))
    if found_left_bones + found_right_bones < 32:
        return False
    return True

def find_matches(mpp_bones: list, rig_bones: list, reverse: bool = False) -> dict:
    if reverse:
        mpp_bones.reverse()
        rig_bones.reverse()
    matches = {}
    for id, rig_bone in enumerate(rig_bones):
        if id <= len(mpp_bones) - 1:
            mpp_bone = mpp_bones[id]
            matches[mpp_bone] = rig_bone
    return matches

def get_mediapipe_wrist_match(handedness: str, rig: bpy.types.Object):
    match = {}
    match[mpp_hand.get_wrist_bone()] = find_wrist_bone(handedness, rig)
    return match

def get_mediapipe_thumb_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_thumb_bones())
    rig_bones = list(find_thumb_bones(handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_index_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('index_finger'))
    rig_bones = list(find_index_bones(handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_middle_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('middle_finger'))
    rig_bones = list(find_middle_bones(handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_ring_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('ring_finger'))
    rig_bones = list(find_ring_bones(handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_pinky_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('pinky'))
    rig_bones = list(find_pinky_bones(handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_matches(handedness: str, rig: bpy.types.Object):
    matches = {}
    matches = matches | get_mediapipe_wrist_match(handedness, rig)
    matches = matches | get_mediapipe_thumb_matches(handedness, rig)
    matches = matches | get_mediapipe_index_matches(handedness, rig)
    matches = matches | get_mediapipe_middle_matches(handedness, rig)
    matches = matches | get_mediapipe_ring_matches(handedness, rig)
    matches = matches | get_mediapipe_pinky_matches(handedness, rig)
    return matches
