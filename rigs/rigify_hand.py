import bpy
from ..mediapipe import hand as mpp_hand

def get_suffix(handedness: str = 'left') -> str:
    suffixes = {'left': 'L', 'right': 'R'}
    return suffixes[handedness]

def finger_bones(finger: str, handedness: str = 'left'):
    for number in ['01', '02', '03']:
        yield "{finger}.{number}.{suffix}".format(
            finger=finger,
            number=number,
            suffix=get_suffix(handedness))
    yield "{finger}.01.{suffix}.001".format(
        finger=finger,
        suffix=get_suffix(handedness))

def find_wrist_bone(handedness: str, rig: bpy.types.Object):
    for bone_pattern in ['hand_ik.{suffix}', 'hand.{suffix}']:
        bone_name = bone_pattern.format(suffix=get_suffix(handedness))
        if bone_name in rig.pose.bones:
            return bone_name

def find_prefixed_finger_bones(prefixed_finger: str, handedness: str, rig: bpy.types.Object):
    for bone_name in finger_bones(prefixed_finger, handedness):
        if bone_name in rig.pose.bones:
            yield bone_name

def find_finger_bones(finger: str, handedness: str, rig: bpy.types.Object):
    prefix_found = False
    for bone_name in find_prefixed_finger_bones('f_' + finger, handedness, rig):
        prefix_found = True
        yield bone_name
    if prefix_found:
        return
    for bone_name in find_prefixed_finger_bones('finger_' + finger, handedness, rig):
        yield bone_name

def find_bones(handedness: str, rig: bpy.types.Object):
    yield find_wrist_bone(handedness, rig)
    yield from find_prefixed_finger_bones('thumb', handedness, rig)
    yield from find_finger_bones('index', handedness, rig)
    yield from find_finger_bones('middle', handedness, rig)
    yield from find_finger_bones('ring', handedness, rig)
    yield from find_finger_bones('pinky', handedness, rig)

def is_a_valid_armature(rig: bpy.types.Object):
    if not rig:
        return False
    if not rig.type == 'ARMATURE':
        return False
    if not hasattr(rig.data, "bones"):
        return False
    found_left_bones = sum(1 for x in find_bones('left', rig))
    found_right_bones = sum(1 for x in find_bones('right', rig))
    if found_left_bones + found_right_bones < 32:
        return False
    return True

def find_matches(mpp_bones: list, rig_bones: list) -> dict:
    mpp_bones.reverse()
    rig_bones.reverse()
    matches = {}
    for id, rig_bone in enumerate(rig_bones):
        if id <= len(mpp_bones) - 1:
            mpp_bone = mpp_bones[id]
            matches[mpp_bone] = rig_bone
    return matches

def get_mediapipe_wrist_match(handedness: str, rig: bpy.types.Object):
    match = {}
    match[mpp_hand.get_wrist_bone()] = find_wrist_bone(handedness, rig)
    return match

def get_mediapipe_thumb_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_thumb_bones())
    rig_bones = list(find_prefixed_finger_bones('thumb', handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_index_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('index_finger'))
    rig_bones = list(find_finger_bones('index', handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_middle_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('middle_finger'))
    rig_bones = list(find_finger_bones('middle', handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_ring_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('ring_finger'))
    rig_bones = list(find_finger_bones('ring', handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_pinky_matches(handedness: str, rig: bpy.types.Object):
    mpp_bones = list(mpp_hand.get_finger_bones('pinky'))
    rig_bones = list(find_finger_bones('pinky', handedness, rig))
    return find_matches(mpp_bones, rig_bones)

def get_mediapipe_matches(handedness: str, rig: bpy.types.Object):
    matches = {}
    matches = matches | get_mediapipe_wrist_match(handedness, rig)
    matches = matches | get_mediapipe_thumb_matches(handedness, rig)
    matches = matches | get_mediapipe_index_matches(handedness, rig)
    matches = matches | get_mediapipe_middle_matches(handedness, rig)
    matches = matches | get_mediapipe_ring_matches(handedness, rig)
    matches = matches | get_mediapipe_pinky_matches(handedness, rig)
    return matches
